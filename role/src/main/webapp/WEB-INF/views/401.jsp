<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.request.contextPath}/dashboard">Home</a></li>
	<li class="breadcrumb-item active">Form 4 Data</li>
</ol>

<div id="layoutError">
	<div id="layoutError_content">
		<main>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<div class="text-center mt-4">
							<h1 class="display-1">401</h1>
							<p class="lead">Unauthorized</p>
							<p>Access to this resource is denied.</p>
							<a href="${pageContext.request.contextPath}/dashboard"><i
								class="fas fa-arrow-left mr-1"></i>Return to Dashboard</a>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
</div>