<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<head>
<link href="<c:url value='/static/css/regis.css' />" rel="stylesheet"
	type="text/css"></link>

<script src="<c:url value='/static/js/global.js' />"
	type="text/javascript"></script>
</head>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.request.contextPath}/dashboard">Home</a></li>
	<li class="breadcrumb-item active">Create User</li>
</ol>

<c:if test="${wrongPassword != null}">

	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${wrongPassword}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

</c:if>

<c:if test="${error != null}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${error}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<c:if test="${roleisempty != null}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${roleisempty}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<form:form method="POST" modelAttribute="user" class="form-horizontal">
	<form:input type="hidden" path="id" id="id" />

	<div class="wrapper wrapper--w960">
		<div class="card card-2">
			<div class="card-body">
				<h2 class="title">Registration From</h2>
				<div class="input-group">
					<c:choose>
						<c:when test="${edit}">
							<input type="text" name="ssoId" value="${user.ssoId}"
								readonly="readonly" type="text" id="ssoId"
								class="input--style-2">
						</c:when>
						<c:otherwise>
							<form:input type="text" path="ssoId" id="ssoId"
								placeholder="User Name" class="input--style-2" />
							<div class="has-error">
								<form:errors path="ssoId" class="help-inline" />
							</div>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="input-group">
					<form:input type="text" path="firstName" id="firstName"
						placeholder="First Name" class="input--style-2" />
					<div class="has-error">
						<form:errors path="firstName" class="help-inline" />
					</div>
				</div>
				<div class="input-group">
					<form:input type="text" path="lastName" id="lastName"
						placeholder="last Name" class="input--style-2" />
					<div class="has-error">
						<form:errors path="lastName" class="help-inline" />
					</div>
				</div>
				<div class="input-group">
					<form:input type="password" path="password" id="password"
						placeholder="Password" class="input--style-2" />
					<div class="has-error">
						<form:errors path="password" class="help-inline" />
					</div>
				</div>
				<div class="input-group">
					<form:input type="password" path="password2" id="password2"
						placeholder="Confirm Password" class="input--style-2" />
					<div class="has-error">
						<form:errors path="password2" class="help-inline" />
					</div>
				</div>
				<div class="input-group">
					<form:input type="text" path="email" id="email" placeholder="Email"
						class="input--style-2" />
					<div class="has-error">
						<form:errors path="email" class="help-inline" />
					</div>
				</div>
				<div class="row row-space">
					<div class="form-group">
						<div class="col-2">
							<div class="input-group">
								<div class="rs-select2 js-select-simple select--no-search">
									<form:select path="userProfiles" multiple="true" itemValue="id"
										itemLabel="type" name="roles" id="roles" class="form-control">
										<option disabled="disabled" selected="selected" value="">---
											SELECT ROLE ---</option>
										<c:forEach items="${roles}" var="status">
											<option value="${status.id}">${status.type}</option>
										</c:forEach>
									</form:select>
									<div class="select-dropdown"></div>
									<div class="has-error">
										<form:errors path="userProfiles" class="help-inline" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<c:choose>
							<c:when test="${edit}">
								<button type="submit" class="btn btn-primary">Update</button>
								<button type="button" onClick="location.href='userList.html'"
									class="btn btn-danger ">Cancel</button>
							</c:when>
							<c:otherwise>
								<button type="submit" class="btn btn-primary">Register</button>
								<button type="button" onClick="location.href='userList.html'"
									class="btn btn-danger ">Cancel</button>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
</form:form>
<br>
<br>