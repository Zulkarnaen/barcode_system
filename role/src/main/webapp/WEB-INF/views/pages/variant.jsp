<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.request.contextPath}/dashboard">Home</a></li>
	<li class="breadcrumb-item active">Variant List</li>
</ol>

<form action="newvariant.htm" method="get">
	<div class="row">

		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').DataTable({
				/* dom : 'Bfrtip',
				buttons : [ {
					extend : 'excelHtml5',
					title : 'Data User'
				}, {
					extend : 'pdfHtml5',
					title : 'Data User',
					orientation: 'landscape',
				    pageSize: 'LEGAL'
				} ] */
				});
			});
		</script>
	</div>
	<!-- DataTables Example -->
	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-table"></i> Data Table Example
		</div>

		<c:if test="${param.success != null}">
			<div class="alert alert-success alert-dismissible fade show"
				role="alert">
				<strong>Success! </strong> ${success}
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<c:if test="${param.error != null}">
			<div class="alert alert-danger alert-dismissible fade show"
				role="alert">
				<strong>Danger! </strong> ${error}
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<c:if test="${param.successHapus != null}">
			<div class="alert alert-success alert-dismissible fade show"
				role="alert">
				<strong>Success! </strong> Data Berhasil di hapus.
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<c:if test="${param.errorHapus != null}">
			<div class="alert alert-danger alert-dismissible fade show"
				role="alert">
				<strong>Danger! </strong> Data tidak dapat dihapus (Kesalahan
				System).
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<c:if test="${success != null}">
			<div class="alert alert-success alert-dismissible fade show"
				role="alert">
				<strong>Success! </strong> ${success}
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<div class="card-body">
			<div class="table-responsive">
				<table id="example" class="table table-striped table-bordered"
					style="width: 100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Created Date</th>
							<th>Variant Name</th>
							<th>Status</th>
							<th>Barcode Length</th>
							<th>Special Character</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${models}" var="user" varStatus="number">
							<!-- 6 total -->
							<tr>
								<td>${fn:escapeXml(user.id)}</td>
								<td>${fn:escapeXml(user.createdDate)}</td>
								<td>${fn:escapeXml(user.variant_name)}</td>
								<td>${fn:escapeXml(user.status)}</td>
								<td>${fn:escapeXml(user.barcode_lenght)}</td>
								<td>${fn:escapeXml(user.special_character)}</td>
								<td><a href="<c:url value='/editvariant${user.id}' />"
									class="btn btn-success custom-width">EDIT</a></td>
								<td><a href="<c:url value='/deletevariant${user.id}' />"
									class="btn btn-danger custom-width">DELETE</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<div class="btn-group">
				<button class="btn btn-primary" type="submit">ADD</button>
			</div>
		</div>
	</div>
</form>

