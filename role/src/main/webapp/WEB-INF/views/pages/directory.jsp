<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<head>
<link href="<c:url value='/static/css/regis.css' />" rel="stylesheet"
	type="text/css"></link>

<script src="<c:url value='/static/js/global.js' />"
	type="text/javascript"></script>
</head>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.request.contextPath}/dashboard">Home</a></li>
	<li class="breadcrumb-item active">Create User</li>
</ol>


<c:if test="${param.error != null}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${error}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<c:if test="${error != null}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${error}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<form method="POST" class="form-horizontal">
	<input type="hidden" id="id" name="id" />

	<div class="wrapper wrapper--w960">
		<div class="card card-2">
			<div class="card-body">
				<h2 class="title">Directory Folder</h2>

				<!-- ID -->
				<input type="hidden" id="ids" name="ids" value=${directorydata.id } />

				<!-- FOLDER -->
				<div class="input-group">
					<input type="text" size="50" id="folder" name="folder"
						placeholder="Folder Name" class="input--style-2"
						value=${directorydata.folderPath } />
				</div>

				<br>

				<h5 class="title" style="text-align: center; color: green;">*untuk
					directory, copy path dari drive yang anda pilih contoh :
					"D:\folderResult\FolderSubResult"</h5>

				<div class="control-group">
					<div class="controls">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />

						<button type="submit" class="btn btn-primary">Register</button>
						<button type="button" onClick="location.href='variantlist.html'"
							class="btn btn-danger ">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<br>
<br>