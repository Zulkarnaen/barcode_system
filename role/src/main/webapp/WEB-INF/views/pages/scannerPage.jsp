<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<head>
<link href="<c:url value='/static/css/regis.css' />" rel="stylesheet"
	type="text/css"></link>

<script src="<c:url value='/static/js/global.js' />"
	type="text/javascript"></script>
</head>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.request.contextPath}/dashboard">Home</a></li>
	<li class="breadcrumb-item active">Scanning Page</li>
</ol>

<c:if test="${error != null}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${error}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<c:if test="${fn:contains(statuscsv, 'failed')}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${statuscsv}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<c:if test="${fn:contains(statuscsv, 'success')}">
	<div class="alert alert-success alert-dismissible fade show"
		role="alert">
		<strong>Success! </strong> ${statuscsv}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<form:form method="POST" class="form-horizontal">

	<div class="wrapper wrapper--w960">
		<div class="card card-3">
			<div class="card-body">
				<h6 class="title"
					style="-webkit-text-decoration-line: underline; text-decoration-line: underline;">Welcome
					${name}</h6>
				<h2 class="title" style="text-align: center">TRACEBILITY SYSTEM</h2>
				<div class="input-group3">
					<input type="text" id="barcode" name="barcode"
						placeholder="Please Scan Your Barcode Here" class="input--style-3" />
				</div>
				<br> <br>

				<c:choose>
					<c:when test="${fn:contains(status, 'failed')}">
						<h4 class="title" style="text-align: center; color: red;">${status }</h4>
					</c:when>
					<c:otherwise>
						<h4 class="title" style="text-align: center; color: green;">${status }</h4>
					</c:otherwise>
				</c:choose>
				<button type="button" onclick="location.href='result.htm';"
					class="btn btn-primary btn-circle btn-xl">${counting }</button>
				<br>
				<h5 class="title" style="text-align: center; color: green;">*Data
					from today on ${datenow }</h5>
				<br> <br>
			</div>
		</div>
	</div>
</form:form>
<br>
<br>