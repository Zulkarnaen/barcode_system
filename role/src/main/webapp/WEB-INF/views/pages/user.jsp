<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.request.contextPath}/dashboard">Home</a></li>
	<li class="breadcrumb-item active">User List</li>
</ol>

<form action="newuser.htm" method="get">
	<div class="row">

		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').DataTable({
				/* dom : 'Bfrtip',
				buttons : [ {
					extend : 'excelHtml5',
					title : 'Data User'
				}, {
					extend : 'pdfHtml5',
					title : 'Data User',
					orientation: 'landscape',
				    pageSize: 'LEGAL'
				} ] */
				});
			});
		</script>
	</div>
	<!-- DataTables Example -->
	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-table"></i> Data Table Example
		</div>

		<c:if test="${param.success != null}">
			<div class="alert alert-success alert-dismissible fade show"
				role="alert">
				<strong>Success! </strong> Data Berhasil di hapus.
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<c:if test="${param.error != null}">
			<div class="alert alert-danger alert-dismissible fade show"
				role="alert">
				<strong>Danger! </strong> Data tidak dapat dihapus (Kesalahan
				System).
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<c:if test="${success != null}">
			<div class="alert alert-success alert-dismissible fade show"
				role="alert">
				<strong>Success! </strong> ${success}
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</c:if>

		<div class="card-body">
			<div class="table-responsive">
				<table id="example" class="table table-striped table-bordered"
					style="width: 100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>User Name</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Level</th>
							<th>Status</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${models}" var="user" varStatus="number">
							<!-- 6 total -->
							<tr>
								<td>${fn:escapeXml(user.id)}</td>
								<td>${fn:escapeXml(user.ssoId)}</td>
								<td>${fn:escapeXml(user.firstName)}</td>
								<td>${fn:escapeXml(user.lastName)}</td>
								<td>${fn:escapeXml(user.email)}</td>
								<td>${fn:escapeXml(user.level)}</td>
								<td>${fn:escapeXml(user.state)}</td>
								<td><a href="<c:url value='/edit${user.id}' />"
									class="btn btn-success custom-width">EDIT</a></td>
								<td><a href="<c:url value='/delete${user.id}' />"
									class="btn btn-danger custom-width">DELETE</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<div class="btn-group">
				<button class="btn btn-primary" type="submit">ADD</button>
			</div>
		</div>
	</div>
</form>

