<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.request.contextPath}/dashboard">Home</a></li>
	<li class="breadcrumb-item active">User List</li>
</ol>

<form:form method="POST" modelAttribute="result" class="form-horizontal">
	<div class="row">

		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').DataTable({
					dom : 'Bfrtip',
					buttons : [ {
						extend : 'excelHtml5',
						title : 'Result Data'
					}, {
						extend : 'pdfHtml5',
						title : 'Result Data',
						/* orientation : 'landscape', */
						pageSize : 'LEGAL'
					} ]
				});
			});
		</script>
	</div>
	<!-- DataTables Example -->
	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-table"></i> Data Table Example
		</div>

		<c:if test="${param.success != null}">
			<div class="alert alert-success">
				<p>Data Berhasil di hapus.</p>
			</div>
		</c:if>

		<c:if test="${param.error != null}">
			<div class="alert alert-danger">
				<p>Data tidak dapat dihapus (Kesalahan System).</p>
			</div>
		</c:if>

		<c:if test="${success != null}">
			<div class="alert alert-success">
				<p>${success}</p>
			</div>
		</c:if>

		<div class="card-body">
			<div class="table-responsive">
				<table id="example" class="table table-striped table-bordered"
					style="width: 100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Created Date</th>
							<th>Serial No</th>
							<th>Variant Name</th>
							<th>User Name</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${models}" var="user" varStatus="number">
							<!-- 6 total -->
							<tr>
								<td>${fn:escapeXml(user.id)}</td>
								<td>${fn:escapeXml(user.createdDate)}</td>
								<td>${fn:escapeXml(user.serialNumber)}</td>
								<td>${fn:escapeXml(user.variantName)}</td>
								<td>${fn:escapeXml(user.userName)}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</div>
	</div>
</form:form>

