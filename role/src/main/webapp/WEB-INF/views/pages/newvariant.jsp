<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<head>
<link href="<c:url value='/static/css/regis.css' />" rel="stylesheet"
	type="text/css"></link>

<script src="<c:url value='/static/js/global.js' />"
	type="text/javascript"></script>
</head>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.request.contextPath}/dashboard">Home</a></li>
	<li class="breadcrumb-item active">Create User</li>
</ol>

<c:if test="${param.success != null}">
	<div class="alert alert-success alert-dismissible fade show"
		role="alert">
		<strong>Success! </strong> ${success}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<c:if test="${param.error != null}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${error}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<c:if test="${wrongPassword != null}">

	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${wrongPassword}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

</c:if>

<c:if test="${error != null}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${error}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<c:if test="${roleisempty != null}">
	<div class="alert alert-danger alert-dismissible fade show"
		role="alert">
		<strong>Danger! </strong> ${roleisempty}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<form method="POST" class="form-horizontal">
	<input type="hidden" id="id" name="id" />

	<div class="wrapper wrapper--w960">
		<div class="card card-2">
			<div class="card-body">
				<h2 class="title">Variant Registration</h2>
				<div class="input-group">

					<c:choose>
						<c:when test="${edit}">
							<input type="text" size="50" id="variant_name"
								name="variant_name" placeholder="Variant Name"
								class="input--style-2" value=${variant.variant_name } />
						</c:when>
						<c:otherwise>
							<input type="text" id="variant_name" name="variant_name"
								placeholder="Variant Name" class="input--style-2" />
						</c:otherwise>
					</c:choose>
				</div>
				<c:choose>
					<c:when test="${edit}">
						<div class="form-group">
							<select class="form-control" name="status" id="status">
								<option disabled="disabled" selected="selected" value="">---
									SELECT STATUS ---</option>
								<c:forEach items="${fn:escapeXml(statusList)}" var="status">
									<c:choose>
										<c:when test="${fn:contains(status, 'status')}">
											<option value="${fn:escapeXml(status)}">
												<c:out value="${fn:escapeXml(status) }" />
											</option>
										</c:when>
										<c:otherwise>
											<option value="${fn:escapeXml(status)}">
												<c:out value="${fn:escapeXml(status) }" />
											</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>

				<c:choose>
					<c:when test="${edit}">
						<div class="input-group">
							<input type="text" id="barcode_lenght" name="barcode_lenght"
								placeholder="Barcode Lenght" class="input--style-2"
								value="${variant.barcode_lenght }" />
						</div>
					</c:when>
					<c:otherwise>
						<div class="input-group">
							<input type="text" id="barcode_lenght" name="barcode_lenght"
								placeholder="Barcode Lenght" class="input--style-2" />
						</div>
					</c:otherwise>
				</c:choose>

				<c:choose>
					<c:when test="${edit}">
						<div class="input-group">
							<input type="text" id="special_character"
								name="special_character" placeholder="Special Character"
								class="input--style-2" value="${variant.special_character }" />
						</div>
					</c:when>
					<c:otherwise>
						<div class="input-group">
							<input type="text" id="special_character"
								name="special_character" placeholder="Special Character"
								class="input--style-2" />
						</div>
					</c:otherwise>
				</c:choose>

				<div class="control-group">
					<div class="controls">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<c:choose>
							<c:when test="${edit}">
								<button type="submit" class="btn btn-primary">Update</button>
								<button type="button" onClick="location.href='variantlist.html'"
									class="btn btn-danger ">Cancel</button>
							</c:when>
							<c:otherwise>
								<button type="submit" class="btn btn-primary">Register</button>
								<button type="button" onClick="location.href='variantlist.html'"
									class="btn btn-danger ">Cancel</button>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<br>
<br>