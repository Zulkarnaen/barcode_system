<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<head>
<link href="<c:url value='/static/css/buble-background.css' />"
	rel="stylesheet" type="text/css"></link>
</head>
<ul class="bubble-boxes">
	<li></li>
	<li></li>
	<li></li>
	<li></li>
	<li></li>
	<li></li>
	<li></li>
	<li></li>
	<li></li>
	<li></li>
</ul>
<div class="card card-login mx-auto mt-5">
	<div class="card-header">SELAMAT DATANG</div>
	<div class="card-body">
		<c:url var="loginUrl" value="/login" />
		<form action="${loginUrl}" method="post">

			<c:if test="${param.logout != null}">
				<div class="alert alert-success">
					<p>Kamu Berhasil Logout.</p>
				</div>
			</c:if>

			<c:if test="${param.error != null}">
				<div class="alert alert-danger">
					<p>Username atau Password Salah.</p>
				</div>
			</c:if>
			
			<c:if test="${param.timeout != null}">
				<div class="alert alert-danger">
					<p>Sesi Anda Telah Berakhir.</p>
				</div>
			</c:if>

			<!-- changed on 09-03-2020 start here -->
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Username"
					required="required" autofocus="autofocus" id="username"
					name="ssoId">
			</div>
			<div class="form-group">
				<input type="password" class="form-control" placeholder="Password"
					required="required" id="password" name="password"> <input
					type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</div>
			<!-- changed on 09-03-2020 until here -->

			<input class="btn btn-primary btn-block" type="submit" value="LOG IN">
			<br>
			<div class="alert alert-secondary">
				<p>Silahkan Masukan Username dan Password.</p>
			</div>
		</form>
	</div>
</div>
