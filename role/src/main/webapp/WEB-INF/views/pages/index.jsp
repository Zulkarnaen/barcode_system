<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>


<c:if test="${success != null}">
	<div class="alert alert-success alert-dismissible fade show"
		role="alert">
		<strong>Success! </strong> ${success}
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</c:if>

<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item active">Dashboard</li>
</ol>

<!-- Page Content -->
<h1 class="display-1" style="text-align: center;">WELCOME
	${username}</h1>
<p class="lead" style="text-align: center;">
	<!-- Page not found. You can <a href="javascript:history.back()">go back</a>
	to the previous page, or <a href="index.html">return home</a>.  -->
	Click the Scanner Page if You Want to Scan Barcode :)
</p>
<br>
<h4 class="title" style="color: green;">Last Update: ${lastupdate }</h4>
<!-- Content Row -->
<div class="row">
	<!-- PENDAPATAN (PERDAY) -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-primary shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2" style="font-size: 25px;">
						<div
							class="text-xs font-weight-bold text-primary text-uppercase mb-1">PROD
							PER (DAY)</div>
						<div class="h1 mb-0 font-weight-bold text-gray-800">${day}</div>
					</div>
					<div class="col-auto">
						<i class="far fa-hourglass fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- PENDAPATAN (WEEKLY) -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-success shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2" style="font-size: 25px;">
						<div
							class="text-xs font-weight-bold text-success text-uppercase mb-1">PROD
							PER (WEEK)</div>
						<div class="h1 mb-0 font-weight-bold text-gray-800">${week}</div>
					</div>
					<div class="col-auto">
						<i class="far fa-clock fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- PENDAPATAN (Monthly) -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-info shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2" style="font-size: 25px;">
						<div
							class="text-xs font-weight-bold text-info text-uppercase mb-1">PROD
							PER (MONTH)</div>
						<div class="h1 mb-0 font-weight-bold text-gray-800">${month}</div>
					</div>
					<div class="col-auto">
						<i class="far fa-calendar fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- PENDAPATAN (Monthly) -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-warning shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2" style="font-size: 25px;">
						<div
							class="text-xs font-weight-bold text-warning text-uppercase mb-1">PROD
							PER (ALL)</div>
						<div class="h1 mb-0 font-weight-bold text-gray-800">${alldata}</div>
					</div>
					<div class="col-auto">
						<i class="far fa-calendar-alt fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>