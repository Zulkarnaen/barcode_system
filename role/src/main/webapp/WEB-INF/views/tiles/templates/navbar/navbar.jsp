<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<ul class="sidebar navbar-nav">

	<!-- DASHBOARD -->
	<li class="nav-item"><a class="nav-link"
		href="${pageContext.request.contextPath}/dashboard"> <i
			class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
	</a></li>

	<!-- SCANNER -->
	<sec:authorize access="hasRole('ADMIN') or hasRole('OPERATOR')">
		<li class="nav-item"><a class="nav-link"
			href="${pageContext.request.contextPath}/scanner"> <i
				class="fas fa-fw fa-camera"></i> <span>Scanner</span>
		</a></li>
	</sec:authorize>

	<!-- RESULT -->
	<sec:authorize access="hasRole('ADMIN') or hasRole('OPERATOR')">
		<li class="nav-item"><a class="nav-link"
			href="${pageContext.request.contextPath}/result"> <i
				class="fas fa-fw fa-table"></i> <span>Result List</span>
		</a></li>
	</sec:authorize>

	<!-- VARIANT -->
	<sec:authorize access="hasRole('ADMIN')">
		<li class="nav-item"><a class="nav-link"
			href="${pageContext.request.contextPath}/variantlist"> <i
				class="fas fa-fw fa-table"></i> <span>Variant List</span>
		</a></li>
	</sec:authorize>


	<!-- HASH ROLE ADMIN PAGE (CREATE NEW USER) -->
	<sec:authorize access="hasRole('ADMIN')">
		<li class="nav-item"><a class="nav-link"
			href="${pageContext.request.contextPath}/userList"> <i
				class="fas fa-fw fa-table"></i> <span>User List</span>
		</a></li>
	</sec:authorize>

	<!-- HASH ROLE ADMIN PAGE (DIRECTORY) -->
	<sec:authorize access="hasRole('ADMIN')">
		<li class="nav-item"><a class="nav-link"
			href="${pageContext.request.contextPath}/directory"> <i
				class="fas fa-fw fa-folder"></i> <span>Directory CSV</span>
		</a></li>
	</sec:authorize>

</ul>
