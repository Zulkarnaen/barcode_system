<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>

<footer class="sticky-footer">
	<div class="container my-auto">
		<div class="copyright text-center my-auto" style="font-size: 18px;">
			<span>Copyright � PT FactoryBase.IO</span>
		</div>
	</div>
</footer>
