<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/common/taglibs.jsp"%>


<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

	<a class="navbar-brand mr-1"
		href="${pageContext.request.contextPath}/dashboard">TRACEBILITY
		SYSTEM</a>

	<button class="btn btn-link btn-sm text-white order-1 order-sm-0"
		id="sidebarToggle" href="#">
		<i class="fas fa-bars"></i>
	</button>

	<!-- Navbar -->
	<ul class="navbar-nav d-md-inline-block form-inline ml-auto">

		<li class="nav-item dropdown no-arrow"><a
			class="nav-link dropdown-toggle" href="#" id="userDropdown"
			role="button" data-toggle="dropdown" aria-haspopup="true"
			aria-expanded="false"> <i class="fas fa-user-circle fa-fw"></i>
		</a>
			<div class="dropdown-menu dropdown-menu-right"
				aria-labelledby="userDropdown">
				<!-- <a class="dropdown-item">HaLLo</a>
				<div class="dropdown-divider"></div>-->
				<a class="dropdown-item" href="<c:url value="/logout" />"
					data-toggle="modal" data-target="#logoutModal">Log out</a>
			</div></li>
	</ul>
</nav>