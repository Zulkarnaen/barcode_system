package com.zulkarnaen.login;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 
 * @author zulkarnaen
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * 
	 * @param request
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public String handleSQLException(HttpServletRequest request, Exception ex) {
		System.out.println("Exception Occured:: URL=" + request.getRequestURL());

		ex.printStackTrace();
		return "error";
	}

	/* TODO: SET HANDLE IO EXCEPTION */
	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "IOException occured")
	@ExceptionHandler(IOException.class)
	public void handleIOException() {
		System.out.println("IOException handler executed");
	}
}