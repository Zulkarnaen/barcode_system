package com.zulkarnaen.login;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 
 * @author zulkarnaen
 *
 */
@Configuration
@EnableScheduling
public class SchedulerConfig {
	/* Sometimes you can use it and declared in here. just wait for it */

}
