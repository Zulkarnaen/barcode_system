package com.zulkarnaen.login.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.zulkarnaen.login.model.DirectoryCSV;
import com.zulkarnaen.login.model.Result;
import com.zulkarnaen.login.model.User;
import com.zulkarnaen.login.model.UserProfile;
import com.zulkarnaen.login.model.Variant;
import com.zulkarnaen.login.model.Variant.VARIANT_STAT;
import com.zulkarnaen.login.requirement.Util;
import com.zulkarnaen.login.service.AspectService;
import com.zulkarnaen.login.service.ReportService;
import com.zulkarnaen.login.service.UserProfileService;

/**
 * 
 * @author zulkarnaen
 *
 */
@Controller
public class ReportController {

	org.slf4j.Logger LOG_CONTROLLER = LoggerFactory.getLogger("auditcontroller");

	@Autowired
	ReportService reportService;

	@Autowired
	AspectService aspectService;

	@Autowired
	UserProfileService userProfileService;

	/* LOGIN */
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		return "login";
	}

	/* LOGOUT */
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	/*
	 * =============================================================================
	 */

	/* DASHBOARD */
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { "/", "/dashboard" }, method = RequestMethod.GET)
	public String layout(Model model, Authentication authentication) {
		String username = authentication.getName().toUpperCase();
		Date date = new Date();
		String lastupdate = Util.formatDateResult(date);

		/* GET ALL DATA */
		List<Result> alldata = reportService.getResultList(null);
		/* GET DATA PER DAY */
		List<Result> day = reportService.getallDataPerDay(date);
		/* GET DATA PER WEEK */
		List<Result> week = reportService.getallDataPerWeek();
		/* GET DATA PER MONTH */
		List<Result> month = reportService.getallDataPerMonth();

		/*
		 * authentication = SecurityContextHolder.getContext().getAuthentication();
		 * String username = authentication.getName().toUpperCase();
		 * 
		 * Get the username of the logged in user: getPrincipal() Get the password of
		 * the authenticated user: getCredentials() Get the assigned roles of the
		 * authenticated user: getAuthorities() Get further details of the authenticated
		 * user: getDetails()
		 */
		model.addAttribute("lastupdate", lastupdate);
		model.addAttribute("username", username);
		model.addAttribute("alldata", alldata.size());
		model.addAttribute("day", day.size());
		model.addAttribute("week", week.size());
		model.addAttribute("month", month.size());
		LOG_CONTROLLER.info("User " + username + " Login in the this website on " + new Date());

		return "index";

	}

	/*
	 * =============================================================================
	 */

	/* SCAN BARCODE GET */
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/scanner", method = RequestMethod.GET)
	public String getScannerPage(Model model) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName().toUpperCase();

		Date date = new Date();
		String dateString = Util.formatDateResulttoString2(date);

		/* COUNTING DATA FROM TODAY */
		List<Result> result = reportService.getResultList(date);

		/* CUSTOM DATA */
		int countingData = result.size();

		User user = new User();
		model.addAttribute("datenow", dateString);
		model.addAttribute("user", user);
		model.addAttribute("counting", countingData);
		model.addAttribute("name", username);
		model.addAttribute("status", "Server Ready!");
		return "scannerPage";

	}

	/* SCANNER BARCODE POST */
	/**
	 * 
	 * @param response
	 * @param model
	 * @param barcode
	 * @return
	 */
	@RequestMapping(value = "/scanner", method = RequestMethod.POST)
	public String getScannerPage(HttpServletResponse response, ModelMap model,
			@RequestParam(required = false, value = "barcode") String barcode) {

		boolean resultSave = false;
		Date date = new Date();
		String dateString = Util.formatDateResulttoString2(date);

		/* COUNTING DATA FROM EARLIER */
		List<Result> result = reportService.getResultList(date);

		/* GET USERNAME */
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName().toUpperCase();

		/* LOGIC SYSTEM IN HERE */
		/* COMPARE BARCODE WITH VARIANT LIST THEN SAVE IF MATCH */
		resultSave = aspectService.compareBarcode(barcode, username);

		if (resultSave == true) {
			model.addAttribute("status", "Success! Data Saved Succesfully!");
		} else {
			model.addAttribute("status", "failed! Barcodes do not match with variant lists!");
		}

		/* RELOAD DATA LAGI DAN TAMPILKAN DISINI */
		result = reportService.getResultList(date);
		int countingData = result.size();

		model.addAttribute("datenow", dateString);
		model.addAttribute("counting", countingData);
		model.addAttribute("name", username);
		return "scannerPage";

	}

	/* HASIL SCANNER DALAM TABLE */
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/result", method = RequestMethod.GET)
	public String getResultData(Model model) {

		Date date = null;

		/* COUNTING DATA FROM TODAY */
		List<Result> result = reportService.getResultList(date);
		model.addAttribute("models", result);
		return "resultdata";

	}

	/*
	 * =============================================================================
	 */

	/* LIST TABLE VARIANT */
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/variantlist", method = RequestMethod.GET)
	public String getVariantList(Model model) {

		List<Variant> userData = reportService.getVariantList();
		model.addAttribute("models", userData);
		return "variant";

	}

	/* CREATE VARIANT NEW GET */
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/newvariant", method = RequestMethod.GET)
	public String newVariant(ModelMap model) {
		Variant variant = new Variant();

		model.addAttribute("variant", variant);
		return "newvariant";
	}

	/* CREATE VARIANT NEW POST */
	/**
	 * 
	 * @param model
	 * @param variant_name
	 * @param barcode_lenght
	 * @param special_character
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/newvariant", method = RequestMethod.POST)
	public String saveVariant(Model model, @RequestParam(required = false, value = "variant_name") String variant_name,
			@RequestParam(required = false, value = "barcode_lenght") int barcode_lenght,
			@RequestParam(required = false, value = "special_character") String special_character) {
		/* PARAMATER */
		int barcode = barcode_lenght;
		boolean validateSpecialChar = false;

		/* VALIDATE BARCODE LENGHT AND SPECIAL CHARACTER LENGHT */
		if (barcode != special_character.length()) {

			LOG_CONTROLLER.error("BARCODE LENGTH DAN PANJANG SPECIAL CHARACTER TIDAK SESUAI");
			model.addAttribute("error", "BARCODE LENGTH DAN PANJANG SPECIAL CHARACTER TIDAK SESUAI!");

			return "newvariant";
		}

		/* TRY TO VALIDATE SPECIAL CHARACTER */
		validateSpecialChar = reportService.validateSpecialCharacter(special_character);

		if (validateSpecialChar == true) {

			LOG_CONTROLLER.error("Special Character Sudah Terdaftar di Database!");
			model.addAttribute("error", "SPECIAL CHARACTER SUDAH TERDAFTAR DI DATABASE!");

			return "newvariant";
		}

		/* FROM PARAM */
		Variant variant = new Variant();
		variant.setCreatedDate(new Date());
		variant.setVariant_name(variant_name);
		variant.setBarcode_lenght(barcode_lenght);
		variant.setSpecial_character(special_character);
		variant.setStatus("ACTIVE");

		/* TODO: LOG NEW USER */
		LOG_CONTROLLER.info("========== NEW VARIANT CREATED WITH DESCRIPTION =========");
		LOG_CONTROLLER.info("CREATED			: [" + new Date() + "]");
		LOG_CONTROLLER.info("VARIANT NAME			: [" + variant.getVariant_name() + "]");
		LOG_CONTROLLER.info("STATUS			: [" + variant.getStatus() + "]");
		LOG_CONTROLLER.info("BARCODE LENGHT			: [" + variant.getBarcode_lenght() + "]");
		LOG_CONTROLLER.info("SPECIAL CHARACTER			: [" + variant.getSpecial_character() + "]");
		LOG_CONTROLLER.info("==========================================================");

		try {
			/* TODO: SAVE TO DATABASE */
			reportService.saveVariant(variant);
		} catch (Exception e) {
			LOG_CONTROLLER.error("Internal Error gagal dalam memasukan data dengan deskripsi : " + e);
			model.addAttribute("error", "Internal Error gagal dalam memasukan data");

			List<Variant> userData = getVariantListModel();
			model.addAttribute("models", userData);
			return "variant";
		}

		model.addAttribute("success", "Nama Variant " + variant.getVariant_name() + " Berhasil di Daftarkan");

		List<Variant> userData = getVariantListModel();
		model.addAttribute("models", userData);

		return "variant";
	}

	/* EDIT VARIAN GET */
	/**
	 * 
	 * @param id
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/editvariant{id}", method = RequestMethod.GET)
	public String editVariant(@PathVariable Integer id, ModelMap model) throws IOException {

		Variant variant = reportService.findByIdVariant(id);

		List<VARIANT_STAT> statusList = new ArrayList<VARIANT_STAT>(EnumSet.allOf(VARIANT_STAT.class));

		model.addAttribute("variant", variant);
		model.addAttribute("edit", true);
		model.addAttribute("statusList", statusList);
		return "newvariant";
	}

	/* EDIT VARIANT POST */
	/**
	 * 
	 * @param request
	 * @param model
	 * @param id
	 * @param variant_name
	 * @param barcode_lenght
	 * @param special_character
	 * @param status
	 * @return
	 */
	@RequestMapping(value = { "/editvariant{id}" }, method = RequestMethod.POST)
	public String updateVariant(HttpServletRequest request, Model model, @PathVariable Integer id,
			@RequestParam(required = false, value = "variant_name") String variant_name,
			@RequestParam(required = false, value = "barcode_lenght") int barcode_lenght,
			@RequestParam(required = false, value = "special_character") String special_character,
			@RequestParam(required = false, value = "status") String status) {

		/* PARAMATER */
		int barcode = barcode_lenght;
		boolean validateSpecialChar = false;

		/* VALIDATE BARCODE LENGHT AND SPECIAL CHARACTER LENGHT */
		if (barcode != special_character.length()) {

			LOG_CONTROLLER.error("BARCODE LENGTH DAN PANJANG SPECIAL CHARACTER TIDAK SESUAI");
			model.addAttribute("error", "BARCODE LENGTH DAN PANJANG SPECIAL CHARACTER TIDAK SESUAI!");

			setVariantField(model, barcode_lenght, special_character, variant_name);

			return "newvariant";
		}

		if (validateSpecialChar == true) {

			LOG_CONTROLLER.error("Special Character Sudah Terdaftar di Database!");
			model.addAttribute("error", "SPECIAL CHARACTER SUDAH TERDAFTAR DI DATABASE!");

			setVariantField(model, barcode_lenght, special_character, variant_name);

			return "newvariant";
		}

		Variant variant = new Variant();

		try {
			if (status.equals("[ACTIVE")) {
				status = "ACTIVE";
			} else if (status.equals(" INACTIVE]")) {
				status = "INACTIVE";
			}
		} catch (Exception e) {
			LOG_CONTROLLER.error("STATUS TIDAK DI PILIH SAAT EDIT");

			setVariantField(model, barcode_lenght, special_character, variant_name);

			model.addAttribute("error", "Status Harus di Set (disarankan di pilih terakhir)");

			return "newvariant";
		}

		/* FROM PARAM */
		variant.setId(id);
		variant.setCreatedDate(new Date());
		variant.setVariant_name(variant_name);
		variant.setBarcode_lenght(barcode_lenght);
		variant.setSpecial_character(special_character);
		variant.setStatus(status);

		/* TODO: LOG NEW USER */
		LOG_CONTROLLER.info("=========== NEW VARIANT UPDATE WITH DESCRIPTION =========");
		LOG_CONTROLLER.info("CREATED				: [" + new Date() + "]");
		LOG_CONTROLLER.info("VARIANT NAME			: [" + variant.getVariant_name() + "]");
		LOG_CONTROLLER.info("STATUS					: [" + variant.getStatus() + "]");
		LOG_CONTROLLER.info("BARCODE LENGHT			: [" + variant.getBarcode_lenght() + "]");
		LOG_CONTROLLER.info("SPECIAL CHARACTER		: [" + variant.getSpecial_character() + "]");
		LOG_CONTROLLER.info("==========================================================");

		try {
			/* UPDATE */
			reportService.updateUserVariant(variant);
		} catch (Exception e) {

			setVariantField(model, barcode_lenght, special_character, variant_name);

			model.addAttribute("error", "Username Tidak Boleh Sama Dengan Yang sudah Ada");

			return "newvariant";
		}

		model.addAttribute("success", "Variant Name " + variant.getVariant_name() + " Berhasil di Update!");

		List<Variant> userData = getVariantListModel();
		model.addAttribute("models", userData);

		return "variant";
	}

	/**
	 * 
	 * @param model
	 * @param barcodeLength
	 * @param specialCharacter
	 * @param variantName
	 */
	private void setVariantField(Model model, int barcodeLength, String specialCharacter, String variantName) {
		Variant variant = new Variant();
		variant.setBarcode_lenght(barcodeLength);
		variant.setSpecial_character(specialCharacter);
		variant.setVariant_name(variantName);

		List<VARIANT_STAT> statusList = new ArrayList<VARIANT_STAT>(EnumSet.allOf(VARIANT_STAT.class));

		model.addAttribute("variant", variant);
		model.addAttribute("edit", true);
		model.addAttribute("statusList", statusList);
	}

	/* DELETE VARIANT ID */
	/**
	 * 
	 * @param response
	 * @param id
	 * @throws IOException
	 */
	@RequestMapping(value = "/deletevariant{id}", method = RequestMethod.GET)
	public void deleteVariant(HttpServletResponse response, @PathVariable Integer id) throws IOException {

		String url;
		try {
			reportService.deleteUserByIDVariant(id);
			url = "variantlist?successHapus";
			LOG_CONTROLLER.info("Sukses Menghapus Data");
		} catch (Exception e) {
			url = "variantlist?errorHapus";
			LOG_CONTROLLER.error("Salah dalam menghapus data dengan pesan : " + e);
		}
		response.sendRedirect(url);
	}

	/* VARIANT LIST MODEL FUNCTION */
	/**
	 * 
	 * @param model
	 * @return
	 */
	private List<Variant> getVariantListModel() {

		List<Variant> userData = reportService.getVariantList();

		return userData;
	}

	/*
	 * =============================================================================
	 */

	/* GET USER LIST REQUEST MAPPING */
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/userList", method = RequestMethod.GET)
	public String getUserList(Model model) {

		List<User> userData = reportService.getUserList();
		model.addAttribute("models", userData);
		return "user";

	}

	/* GET USER LIST */
	/**
	 * 
	 * @param model
	 * @return
	 */
	private List<User> getUserListModel(Model model) {

		List<User> userData = reportService.getUserList();

		return userData;
	}

	/* CREATE NEW USER GET */
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/newuser", method = RequestMethod.GET)
	public String newRegistration(ModelMap model) {
		User user = new User();
		model.addAttribute("user", user);
		return "newuser";
	}

	/* CREATE NEW USER POST */
	/**
	 * 
	 * @param user
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/newuser", method = RequestMethod.POST)
	public String saveRegistration(@Valid User user, BindingResult result, ModelMap model) {

		/* TODO: GET FIELD ERROR FROM BINDINGRESULT */
		String fieldLevel;
		fieldLevel = result.getFieldError().getField();
		fieldLevel.toLowerCase();

		/* TODO: LOGIC FOR BINDING RESULT */
		if (result.hasErrors()) {
			if (fieldLevel.equals("level")) {

			} else {
				LOG_CONTROLLER.error("Ada data yang kosong yaitu: " + fieldLevel);
				model.addAttribute("error", "Ada data yang kosong yaitu: " + fieldLevel);
				return "newuser";
			}

		} else if (!user.getPassword().equals(user.getPassword2())) {
			LOG_CONTROLLER.error("Password dan Konfirmasi Password tidak sesusai");
			model.addAttribute("wrongPassword", "Password dan Konfirmasi Password tidak sesusai");
			return "newuser";

		}

		/* TODO: LOGIC FOR GET USER PROFILES */
		if (user.getUserProfiles() == null || user.getUserProfiles().isEmpty()) {
			LOG_CONTROLLER.error("Data Role kosong, Pastikan Role dipilih saat semua data sudah di masukan.");
			model.addAttribute("roleisempty",
					"Data Role kosong, Pastikan Role dipilih saat semua data sudah di masukan.");
			return "newuser";
		} else {
			for (UserProfile profile : user.getUserProfiles()) {
				System.out.println("Profile : " + profile.getType());
				user.setLevel(profile.getType());
			}
		}

		/* TODO: LOG NEW USER */
		LOG_CONTROLLER.info("========== NEW USER CREATED ==========");
		LOG_CONTROLLER.info("CREATED	: [" + new Date() + "]");
		LOG_CONTROLLER.info("FIRST NAME	: [" + user.getFirstName() + "]");
		LOG_CONTROLLER.info("LAST NAME	: [" + user.getLastName() + "]");
		LOG_CONTROLLER.info("USERNAME	: [" + user.getSsoId() + "]");
		LOG_CONTROLLER.info("PASSWORD	: [" + user.getPassword() + "]");
		LOG_CONTROLLER.info("EMAIL		: [" + user.getEmail() + "]");
		LOG_CONTROLLER.info("LEVEL		: [" + user.getEmail() + "]");
		LOG_CONTROLLER.info("======================================");

		try {
			/* TODO: SAVE TO DATABASE */
			reportService.save(user);
		} catch (Exception e) {
			LOG_CONTROLLER.error("Username Tidak Boleh Sama Dengan Yang sudah Ada");
			model.addAttribute("error", "Username Tidak Boleh Sama Dengan Yang sudah Ada");
			return "newuser";
		}

		model.addAttribute("success", "User " + user.getFirstName() + " has been registered successfully");
		return "index";
	}

	/* EDIT USER GET */
	/**
	 * 
	 * @param id
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/edit{id}", method = RequestMethod.GET)
	public String editUser(@PathVariable Integer id, ModelMap model) throws IOException {

		User user = reportService.findById(id);

		user.setPassword(null);

		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		return "newuser";
	}

	/* EDIT USER POST */
	/**
	 * 
	 * @param request
	 * @param user
	 * @param result
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = { "/edit{id}" }, method = RequestMethod.POST)
	public String updateUser(HttpServletRequest request, @Valid User user, BindingResult result, Model model,
			@PathVariable Integer id) {

		/* TODO: GET FIELD ERROR FROM BINDINGRESULT */
		String fieldLevel;
		fieldLevel = result.getFieldError().getField();
		fieldLevel.toLowerCase();

		/* TODO: LOGIC FOR BINDING RESULT */
		if (result.hasErrors()) {
			if (fieldLevel.equals("level")) {

			} else {
				LOG_CONTROLLER.error("Ada data yang kosong yaitu: " + fieldLevel);
				model.addAttribute("error", "Ada data yang kosong yaitu: " + fieldLevel);
				return "newuser";
			}

		} else if (!user.getPassword().equals(user.getPassword2())) {
			LOG_CONTROLLER.error("Password dan Konfirmasi Password tidak sesusai");
			model.addAttribute("wrongPassword", "Password dan Konfirmasi Password tidak sesusai");
			return "newuser";

		}

		/* TODO: LOGIC FOR GET USER PROFILES */
		if (user.getUserProfiles() == null || user.getUserProfiles().isEmpty()) {
			LOG_CONTROLLER.error("Data Role kosong, Pastikan Role dipilih saat semua data sudah di masukan.");
			model.addAttribute("roleisempty",
					"Data Role kosong, Pastikan Role dipilih saat semua data sudah di masukan.");
			return "newuser";
		} else {
			for (UserProfile profile : user.getUserProfiles()) {
				System.out.println("Profile : " + profile.getType());
				user.setLevel(profile.getType());
			}
		}

		/* TODO: LOG NEW USER */
		LOG_CONTROLLER.info("========= UPDATE USER CREATED ========");
		LOG_CONTROLLER.info("CREATED	: [" + new Date() + "]");
		LOG_CONTROLLER.info("FIRST NAME	: [" + user.getFirstName() + "]");
		LOG_CONTROLLER.info("LAST NAME	: [" + user.getLastName() + "]");
		LOG_CONTROLLER.info("USERNAME	: [" + user.getSsoId() + "]");
		LOG_CONTROLLER.info("PASSWORD	: [" + user.getPassword() + "]");
		LOG_CONTROLLER.info("EMAIL		: [" + user.getEmail() + "]");
		LOG_CONTROLLER.info("LEVEL		: [" + user.getEmail() + "]");
		LOG_CONTROLLER.info("======================================");

		try {
			/* UPDATE */
			reportService.updateUser(user);
		} catch (Exception e) {
			setUserField(model, id);
			model.addAttribute("error", "Kegagalan Dalam Menyimpan Data");

			return "newuser";
		}

		model.addAttribute("success",
				"User " + user.getFirstName() + " " + user.getLastName() + " has updated successfully");

		List<User> userData = getUserListModel(model);
		model.addAttribute("models", userData);

		return "user";
	}

	private void setUserField(Model model, int id) {
		User user = reportService.findById(id);

		user.setPassword(null);

		model.addAttribute("user", user);
		model.addAttribute("edit", true);
	}

	/* DELETE USER */
	/**
	 * 
	 * @param response
	 * @param id
	 * @throws IOException
	 */
	@RequestMapping(value = "/delete{id}", method = RequestMethod.GET)
	public void deleteUser(HttpServletResponse response, @PathVariable Integer id) throws IOException {

		String url;
		try {
			reportService.deleteUserByID(id);
			url = "userList?success";
			LOG_CONTROLLER.info("Sukses Menghapus Data");
		} catch (Exception e) {
			url = "userList?error";
			LOG_CONTROLLER.error("Salah dalam menghapus data dengan pesan : " + e);
		}
		response.sendRedirect(url);
	}

	/* GET ROLES FOR ROLES */
	/**
	 * 
	 * @return
	 */
	@ModelAttribute("roles")
	public List<UserProfile> initializeProfiles() {
		return userProfileService.findAll();
	}

	/*
	 * =============================================================================
	 */

	/* IF ERROR */
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/error401", method = RequestMethod.GET)
	public String error(Model model) {

		return "error401";

	}

	/*
	 * ============================================================================
	 */

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/directory", method = RequestMethod.GET)
	public String directory(ModelMap model) {

		List<DirectoryCSV> modelDirectory = reportService.getDirectoryList();

		String folderPath = modelDirectory.get(0).getFolderPath().replace("//", "\\");

		DirectoryCSV directorydata = new DirectoryCSV();
		directorydata.setId(modelDirectory.get(0).getId());
		directorydata.setFolderPath(folderPath);

		model.addAttribute("directorydata", directorydata);
		return "directory";
	}

	/* CREATE VARIANT NEW POST */
	/**
	 * 
	 * @param model
	 * @param variant_name
	 * @param barcode_lenght
	 * @param special_character
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/directory", method = RequestMethod.POST)
	public String directory(Model model, @RequestParam(required = false, value = "ids") String ids,
			@RequestParam(required = false, value = "folder") String folder) {

		int id = Integer.parseInt(ids);
		String folderRemake = folder.replace("\\", "//");

		/* FROM PARAM */
		DirectoryCSV directorydata = new DirectoryCSV();
		directorydata.setId(id);
		directorydata.setFolderPath(folderRemake);

		/* TODO: LOG NEW USER */
		LOG_CONTROLLER.info("=========== DIRECTORY CREATED WITH DESCRIPTION ==========");
		LOG_CONTROLLER.info("CREATED		: [" + new Date() + "]");
		LOG_CONTROLLER.info("FOLDER PATH		: [" + directorydata.getFolderPath() + "]");
		LOG_CONTROLLER.info("==========================================================");

		try {
			/* TODO: SAVE TO DATABASE */
			reportService.saveDirectory(directorydata);
		} catch (Exception e) {
			LOG_CONTROLLER.error("Internal Error gagal dalam memasukan data dengan deskripsi : " + e);
			model.addAttribute("error", "Internal Error gagal dalam memasukan data");

			setDirectoryField(model, folder);

			return "directory";
		}

		model.addAttribute("success", "Save Directory Berhasil!");

		return "index";
	}

	/**
	 * 
	 * @param model
	 * @param barcodeLength
	 * @param specialCharacter
	 * @param variantName
	 */
	private void setDirectoryField(Model model, String folder) {

		DirectoryCSV directorydata = new DirectoryCSV();
		directorydata.setFolderPath(folder);

		model.addAttribute("directorydata", directorydata);
	}

}
