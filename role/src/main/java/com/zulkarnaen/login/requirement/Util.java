package com.zulkarnaen.login.requirement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author zulkarnaen
 *
 */
public class Util {
	
	public static final SimpleDateFormat DB_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static Date formatDateTime(String s) {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		if (s == null || s.isEmpty()) {
			return null;
		}
		try {
			Date date;
			date = sf.parse(s);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param param
	 * @return
	 */
	public static String formatDateStart(Date param) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

		Calendar cal = Calendar.getInstance();
		cal.setTime(param);

		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);

		Date newFormatted = cal.getTime();

		String date = sdf.format(newFormatted);

		return date;
	}

	/**
	 * 
	 * @param param
	 * @return
	 */
	public static String formatDateResult(Date param) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

		String date = sdf.format(param);

		return date;
	}

	public static String formatDateResulttoString(Date param) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		String date = sdf.format(param);

		return date;
	}

	public static String formatDateResulttoString2(Date param) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMMMMMM yyyy");

		String date = sdf.format(param);

		return date;
	}

}
