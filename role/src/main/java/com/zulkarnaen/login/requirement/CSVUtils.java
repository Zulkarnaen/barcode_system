package com.zulkarnaen.login.requirement;

import java.io.PrintWriter;
import java.util.List;

import com.zulkarnaen.login.model.Result;

public class CSVUtils {

	public static void downloadCsv(PrintWriter writer, List<Result> results) {
		writer.write("Created Date, Variant Name, Serial Number, User Name \n");
		for (Result result : results) {
			writer.write(result.getCreatedDate() + "," + result.getVariantName() + "," + result.getSerialNumber() + ","
					+ result.getUserName() + "\n");
		}
	}

}
