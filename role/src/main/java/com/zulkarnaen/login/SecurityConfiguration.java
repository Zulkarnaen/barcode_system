package com.zulkarnaen.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 
 * @author zulkarnaen
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
		auth.authenticationProvider(authenticationProvider());
	}

	@Autowired
	private LoggingAccessDeniedHandler accessDeniedHandler;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/* TODO: SET THE AUTHECTICATION PROVIDER */
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider;
	}

	/*
	 * TODO: SET THE CONFIGURE SECURITY WHEN YOU NEED ACCESS SOME URL MUST GIVE A
	 * HASHROLE. IF YOU WANT TO GIVE ACCESS LOGIN, GET USERNAME AND PASSWORD, CSRF,
	 * MAXIMUMSESSION, GIVE ERROR RESPONSE, DEFAULT PAGE, AND LOGIN PAGE
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/dashboard").access("hasRole('ADMIN') or hasRole('OPERATOR')")
				.antMatchers("/role/**").access("hasRole('ADMIN') or hasRole('OPERATOR')").antMatchers("/newuser/**")
				.access("hasRole('ADMIN')").antMatchers("/delete/**").access("hasRole('ADMIN') or hasRole('OPERATOR')")
				.and().formLogin().loginPage("/login").usernameParameter("ssoId").passwordParameter("password").and()
				.csrf().and().exceptionHandling().accessDeniedHandler(accessDeniedHandler);

		http.sessionManagement().maximumSessions(1).expiredUrl("/login?timeout");
		http.formLogin().failureUrl("/login?error").defaultSuccessUrl("/dashboard").loginPage("/login");

	}
}
