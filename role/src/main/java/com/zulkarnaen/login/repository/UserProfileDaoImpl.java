package com.zulkarnaen.login.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.zulkarnaen.login.model.UserProfile;

/**
 * 
 * @author zulkarnaen
 *
 */
@Repository("userProfileDao")
public class UserProfileDaoImpl extends AbstractDao<Integer, UserProfile> implements UserProfileDao {

	/* TODO: FIND ALL DATA WITH TYPE ASC */
	@SuppressWarnings("unchecked")
	public List<UserProfile> findAll() {
		Criteria crit = createEntityCriteria();
		crit.addOrder(Order.asc("type"));
		return (List<UserProfile>) crit.list();
	}

	/* TODO: FIND BY ID */
	public UserProfile findById(int id) {
		return getByKey(id);
	}

	/* TODO: FIND EQUAL BY TYPE */
	public UserProfile findByType(String type) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("type", type));
		return (UserProfile) crit.uniqueResult();
	}
}