package com.zulkarnaen.login.repository;

import com.zulkarnaen.login.model.Variant;

public interface VariantDao {

	public Variant findByIdVariant(int id);

	public void save(Variant user);

	public void deleteByID(int id);

}
