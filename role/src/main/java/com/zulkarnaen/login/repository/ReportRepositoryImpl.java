package com.zulkarnaen.login.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.zulkarnaen.login.requirement.Util;

/**
 * 
 * @author zulkarnaen
 *
 */
@Repository
public class ReportRepositoryImpl implements ReportRepository {

	org.slf4j.Logger LOG_CONTROLLER = LoggerFactory.getLogger("auditservice");

	@Qualifier(value = "entityManager")
	@PersistenceContext
	EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllUserData() {

		/* TODO: SET QUERY */
		String queryStr = "SELECT * FROM app_user";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllResultPerDay(String dateStart, String dateEnd) {
		/* TODO: SET QUERY */
		String queryStr = "SELECT * FROM result_data a WHERE a.createdDate BETWEEN :1 AND :2";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			/* TODO: GIVE PARAMETER IS NEEDED */
			query.setParameter("1", dateStart);
			query.setParameter("2", dateEnd);

			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllResultPerWeek() {
		/* TODO: SET QUERY */
		String queryStr = "select * from result_data a where YEARWEEK(a.createdDate) = yearweek(curdate())";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllResultPerMonth() {
		/* TODO: SET QUERY */
		String queryStr = "select * from result_data a where MONTH(a.createdDate) = MONTH(CURDATE())";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllUserDataResult() {
		/* TODO: SET QUERY */
		String queryStr = "SELECT * FROM result_data";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findAllUserDataResultWithDate(Date date) {
		String dateStart;
		String dateEnd;
		dateStart = Util.formatDateResulttoString(date) + " 00:00:00";
		dateEnd = Util.formatDateResulttoString(date) + " 23:59:59";

		/* TODO: SET QUERY */
		String queryStr = "SELECT * FROM result_data a WHERE a.createdDate BETWEEN :1 AND :2";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			/* TODO: GIVE PARAMETER IS NEEDED */
			query.setParameter("1", dateStart);
			query.setParameter("2", dateEnd);

			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findAllUserDataVariant() {
		/* TODO: SET QUERY */
		String queryStr = "SELECT * FROM variant_data";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findAllUserDataVariantWithActive() {
		/* TODO: SET QUERY */
		String queryStr = "SELECT * FROM variant_data a WHERE a.status = 'ACTIVE'";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findAllDirectoryCSV() {
		/* TODO: SET QUERY */
		String queryStr = "SELECT * FROM directory_data";
		List<Object[]> objectList = null;

		try {
			/* TODO: CREATE QUERY NATIVE */
			Query query = entityManager.createNativeQuery(queryStr);
			objectList = query.getResultList();

		} catch (Exception e) {
			LOG_CONTROLLER.error("GET ERROR WHEN QUERY DATA WITH LOG IS :" + e);
		}

		return objectList;
	}

}
