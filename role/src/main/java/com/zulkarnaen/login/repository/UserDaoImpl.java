package com.zulkarnaen.login.repository;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.zulkarnaen.login.model.User;

/**
 * 
 * @author zulkarnaen
 *
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	/* TODO: SAVE USER */
	public void save(User user) {
		persist(user);
	}

	/* TODO: FIND BY ID */
	public User findById(int id) {
		return getByKey(id);
	}

	/* TODO: FIND BY USERNAME */
	public User findBySSO(String sso) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("ssoId", sso));
		return (User) crit.uniqueResult();
	}

	/* TODO: DELETE BY ID */
	@Override
	public void deleteByID(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		User user = (User) crit.uniqueResult();
		delete(user);

	}

}