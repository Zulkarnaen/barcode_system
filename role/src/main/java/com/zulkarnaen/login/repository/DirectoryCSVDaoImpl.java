package com.zulkarnaen.login.repository;

import org.springframework.stereotype.Repository;

import com.zulkarnaen.login.model.DirectoryCSV;

@Repository("directoryCSVDao")
public class DirectoryCSVDaoImpl extends AbstractDao<Integer, DirectoryCSV> implements DirectoryCSVDao {

	/* TODO: SAVE USER */
	public void save(DirectoryCSV directoryCSV) {
		persist(directoryCSV);
	}

	@Override
	public DirectoryCSV findById(int id) {
		return getByKey(id);
	}

}
