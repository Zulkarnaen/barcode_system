package com.zulkarnaen.login.repository;

import com.zulkarnaen.login.model.User;

/**
 * 
 * @author zulkarnaen
 *
 */
public interface UserDao {

	public User findById(int id);

	public User findBySSO(String sso);

	public void save(User user);

	public void deleteByID(int id);

}
