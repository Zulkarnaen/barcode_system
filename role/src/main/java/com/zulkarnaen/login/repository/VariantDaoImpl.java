package com.zulkarnaen.login.repository;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.zulkarnaen.login.model.Variant;

@Repository("variantDao")
public class VariantDaoImpl extends AbstractDao<Integer, Variant> implements VariantDao {

	/* TODO: SAVE USER */
	public void save(Variant user) {
		persist(user);
	}

	/* TODO: FIND BY ID */
	public Variant findByIdVariant(int id) {
		return getByKey(id);
	}

	@Override
	public void deleteByID(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Variant variant = (Variant) crit.uniqueResult();
		delete(variant);
	}

}
