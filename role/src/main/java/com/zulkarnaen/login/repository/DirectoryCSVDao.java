package com.zulkarnaen.login.repository;

import com.zulkarnaen.login.model.DirectoryCSV;

public interface DirectoryCSVDao {

	public void save(DirectoryCSV directoryCSV);

	public DirectoryCSV findById(int id);

}
