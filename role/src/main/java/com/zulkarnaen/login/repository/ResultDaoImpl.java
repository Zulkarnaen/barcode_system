package com.zulkarnaen.login.repository;

import org.springframework.stereotype.Repository;

import com.zulkarnaen.login.model.Result;

@Repository
public class ResultDaoImpl extends AbstractDao<Integer, Result> implements ResultDao {

	public void save(Result result) {
		persist(result);
	}

}
