package com.zulkarnaen.login.repository;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author zulkarnaen
 *
 */
public interface ReportRepository {

	public List<Object[]> findAllUserData();

	public List<Object[]> findAllUserDataResult();

	public List<Object[]> findAllUserDataVariant();

	public List<Object[]> findAllUserDataVariantWithActive();

	public List<Object[]> findAllUserDataResultWithDate(Date date);

	public List<Object[]> findAllDirectoryCSV();

	public List<Object[]> findAllResultPerDay(String dateStart, String dateEnd);

	public List<Object[]> findAllResultPerWeek();

	public List<Object[]> findAllResultPerMonth();

}