package com.zulkarnaen.login.repository;

import java.util.List;

import com.zulkarnaen.login.model.UserProfile;

/**
 * 
 * @author zulkarnaen
 *
 */
public interface UserProfileDao {

	public List<UserProfile> findAll();

	public UserProfile findByType(String type);

	public UserProfile findById(int id);
}