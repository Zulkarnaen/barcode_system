package com.zulkarnaen.login.repository;

import com.zulkarnaen.login.model.Result;

public interface ResultDao {

	public void save(Result result);

}
