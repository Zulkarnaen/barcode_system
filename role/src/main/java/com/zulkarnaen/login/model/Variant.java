package com.zulkarnaen.login.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VARIANT_DATA")
public class Variant {

	private int id;
	private Date createdDate;
	private String variant_name;
	private String status;
	private int barcode_lenght;
	private String special_character;

	public enum VARIANT_STAT {
		ACTIVE, INACTIVE;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "createdDate")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "variantname")
	public String getVariant_name() {
		return variant_name;
	}

	public void setVariant_name(String variant_name) {
		this.variant_name = variant_name;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "barcodelength")
	public int getBarcode_lenght() {
		return barcode_lenght;
	}

	public void setBarcode_lenght(int barcode_lenght) {
		this.barcode_lenght = barcode_lenght;
	}

	@Column(name = "specialchar")
	public String getSpecial_character() {
		return special_character;
	}

	public void setSpecial_character(String special_character) {
		this.special_character = special_character;
	}

}
