package com.zulkarnaen.login.service;

import java.util.Date;
import java.util.List;

import com.zulkarnaen.login.model.DirectoryCSV;
import com.zulkarnaen.login.model.Result;
import com.zulkarnaen.login.model.User;
import com.zulkarnaen.login.model.Variant;

/**
 * 
 * @author zulkarnaen
 *
 */
public interface ReportService {

	public List<User> getUserList();

	public List<DirectoryCSV> getDirectoryList();

	public List<Result> getResultList(Date date);

	public List<Variant> getVariantList();

	public List<Variant> getVariantListWithActive();

	public User findById(int id);

	public User findBySso(String ssoId);

	public void save(User user);

	public void updateUser(User user);

	public void deleteUserByID(int sso);

	public Variant findByIdVariant(int id);

	public void deleteUserByIDVariant(int id);

	public void saveVariant(Variant variant);

	public void updateUserVariant(Variant variant);

	public boolean validateSpecialCharacter(String specialChar);

	public void saveDirectory(DirectoryCSV directorydata);

	public List<Result> getallDataPerDay(Date date);

	public List<Result> getallDataPerWeek();

	public List<Result> getallDataPerMonth();

}
