package com.zulkarnaen.login.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;
import com.zulkarnaen.login.model.DirectoryCSV;
import com.zulkarnaen.login.model.Result;
import com.zulkarnaen.login.requirement.Util;

@Service("csvService")
public class CsvServiceImpl implements CsvService {

	org.slf4j.Logger LOG_SERVICE = LoggerFactory.getLogger("auditservice");

	@Autowired
	private ReportService reportService;

	public boolean downloadCSV(List<Result> result, Date date, boolean hasil) {

		List<DirectoryCSV> directory = reportService.getDirectoryList();

		/* FOLDER AND FILE DIRECTORY */
		String folderNamePath = directory.get(0).getFolderPath();
		String fileNamePath = "dataCSV_";

		/* SETTING DATE */
		String dates = Util.formatDateResult(date);
		String dateFolder = Util.formatDateResulttoString2(date);

		/* GET FILE NAME WITH DATE */
		String filename = fileNamePath + dates + ".csv";

		/* GET LOCATION */
		String locationPath = folderNamePath + "//" + dateFolder;

		/* TRY TO CREATE FOLDER */
		File theDir = new File(locationPath);

		if (!theDir.exists()) {
			LOG_SERVICE.info("creating directory: " + theDir.getName());
			boolean resultmkdir = false;

			try {
				theDir.mkdirs();
				resultmkdir = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (resultmkdir) {
				LOG_SERVICE.info("Folder created");
			}
		}

		CSVWriter writer;
		try {
			writer = new CSVWriter(new FileWriter(locationPath + "//" + filename));

			// Writing data to a csv file
			String line1[] = { "ID", "CreateDate", "Variant Name", "Serial Number", "User Name" };
			writer.writeNext(line1);
			for (int i = 0; i < result.size(); i++) {

				String id = Integer.toString(result.get(i).getId());
				String dateS = Util.formatDateResulttoString(result.get(i).getCreatedDate());
				String serialNumber = result.get(i).getSerialNumber();
				String userName = result.get(i).getUserName();
				String variantName = result.get(i).getVariantName();

				String lineloop[] = { id, dateS, serialNumber, userName, variantName };
				/* Writing data to the csv file */
				writer.writeNext(lineloop);

			}

			/* Flushing data from writer to file */
			writer.flush();
			LOG_SERVICE.info("CSV BERHASIL DI BUAT");
			hasil = true;

			return hasil;

		} catch (IOException e) {
			LOG_SERVICE.error("FAILED CREATE FILE CSV");
		}

		return hasil;
	}

}
