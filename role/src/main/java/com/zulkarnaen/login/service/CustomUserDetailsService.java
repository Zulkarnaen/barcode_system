package com.zulkarnaen.login.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zulkarnaen.login.model.User;
import com.zulkarnaen.login.model.UserProfile;

/**
 * 
 * @author zulkarnaen
 *
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	org.slf4j.Logger LOG_CONTROLLER = LoggerFactory.getLogger("auditservice");

	@Autowired
	private ReportService reportService;

	/* TODO: LOAD USER BY USERNAME */
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String ssoId) throws UsernameNotFoundException {
		User user = reportService.findBySso(ssoId);
		LOG_CONTROLLER.info("iam logged with User : " + user);
		if (user == null) {
			LOG_CONTROLLER.error("Username Not Found!");
			throw new UsernameNotFoundException("Username not found!");
		}
		return new org.springframework.security.core.userdetails.User(user.getSsoId(), user.getPassword(),
				user.getState().equals("Active"), true, true, true, getGrantedAuthorities(user));
	}

	/**
	 * 
	 * @param user
	 * @return
	 */
	/* TODO: SET AUTHORITIES TO LEVEL USER */
	private List<GrantedAuthority> getGrantedAuthorities(User user) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (UserProfile userProfile : user.getUserProfiles()) {
			LOG_CONTROLLER.info("My Level is -> : " + userProfile);
			authorities.add(new SimpleGrantedAuthority("ROLE_" + userProfile.getType()));
		}
		LOG_CONTROLLER.info("authorities :" + authorities + "\n");
		return authorities;
	}

}