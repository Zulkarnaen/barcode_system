package com.zulkarnaen.login.service;

import java.util.List;

import com.zulkarnaen.login.model.UserProfile;

/**
 * 
 * @author zulkarnaen
 *
 */
public interface UserProfileService {

	public List<UserProfile> findAll();

	public UserProfile findByType(String type);

	public UserProfile findById(int id);
}