package com.zulkarnaen.login.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zulkarnaen.login.model.DirectoryCSV;
import com.zulkarnaen.login.model.Result;
import com.zulkarnaen.login.model.User;
import com.zulkarnaen.login.model.Variant;
import com.zulkarnaen.login.repository.DirectoryCSVDao;
import com.zulkarnaen.login.repository.ReportRepository;
import com.zulkarnaen.login.repository.UserDao;
import com.zulkarnaen.login.repository.VariantDao;
import com.zulkarnaen.login.requirement.Util;

/**
 * 
 * @author zulkarnaen
 *
 */
@Service("reportService")
@Transactional(readOnly = true)
public class ReportServiceImpl implements ReportService {

	org.slf4j.Logger LOG_CONTROLLER = LoggerFactory.getLogger("auditservice");

	@Autowired
	private ReportRepository reportRepository;

	@Autowired
	private UserDao dao;

	@Autowired
	private VariantDao variantDao;

	@Autowired
	private DirectoryCSVDao directoryCSVDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/* TODO: LIST USER */
	@Override
	public List<User> getUserList() {

		List<Object[]> form1Objects = reportRepository.findAllUserData();
		List<User> userModel = new ArrayList<User>();

		try {

			for (Object object[] : form1Objects) {

				User model = new User();
				model.setId((int) object[0]);
				model.setSsoId((String) object[1]);
				model.setFirstName((String) object[3]);
				model.setLastName((String) object[4]);
				model.setEmail((String) object[5]);
				model.setState((String) object[6]);
				model.setLevel((String) object[7]);

				userModel.add(model);
			}

			LOG_CONTROLLER.info("TOTAL USER DATA [" + userModel.size() + " data ]");

		} catch (Exception e) {
			LOG_CONTROLLER.info("Failed Get data with problem is : " + e);
		}

		return userModel;
	}

	@Override
	public List<Result> getallDataPerDay(Date date) {

		/* SET SPESIFIC DATE IN STRING */
		String startDate = Util.DB_DATE_FORMAT.format(date) + " 00:00:00";
		String endDate = Util.DB_DATE_FORMAT.format(date) + " 23:59:59";

		List<Object[]> form1Objects;

		form1Objects = reportRepository.findAllResultPerDay(startDate, endDate);

		List<Result> userModel = new ArrayList<Result>();

		try {

			for (Object object[] : form1Objects) {

				Result model = new Result();
				model.setId((int) object[0]);

				userModel.add(model);
			}

			LOG_CONTROLLER.info("TOTAL RESULT DATA [" + userModel.size() + " data ]");

		} catch (Exception e) {
			LOG_CONTROLLER.info("Failed Get data with problem is : " + e);
		}

		return userModel;
	}

	@Override
	public List<Result> getallDataPerWeek() {
		List<Object[]> form1Objects;

		form1Objects = reportRepository.findAllResultPerWeek();

		List<Result> userModel = new ArrayList<Result>();

		try {

			for (Object object[] : form1Objects) {

				Result model = new Result();
				model.setId((int) object[0]);

				userModel.add(model);
			}

			LOG_CONTROLLER.info("TOTAL RESULT DATA [" + userModel.size() + " data ]");

		} catch (Exception e) {
			LOG_CONTROLLER.info("Failed Get data with problem is : " + e);
		}

		return userModel;
	}

	@Override
	public List<Result> getallDataPerMonth() {
		List<Object[]> form1Objects;

		form1Objects = reportRepository.findAllResultPerMonth();

		List<Result> userModel = new ArrayList<Result>();

		try {

			for (Object object[] : form1Objects) {

				Result model = new Result();
				model.setId((int) object[0]);

				userModel.add(model);
			}

			LOG_CONTROLLER.info("TOTAL RESULT DATA [" + userModel.size() + " data ]");

		} catch (Exception e) {
			LOG_CONTROLLER.info("Failed Get data with problem is : " + e);
		}

		return userModel;
	}

	@Override
	public List<Result> getResultList(Date date) {

		List<Object[]> form1Objects;

		if (date == null) {
			form1Objects = reportRepository.findAllUserDataResult();
		} else {
			form1Objects = reportRepository.findAllUserDataResultWithDate(date);
		}

		List<Result> userModel = new ArrayList<Result>();

		try {

			for (Object object[] : form1Objects) {

				Result model = new Result();
				model.setId((int) object[0]);
				model.setCreatedDate((Date) object[1]);
				model.setSerialNumber((String) object[2]);
				model.setVariantName((String) object[3]);
				model.setUserName((String) object[4]);

				userModel.add(model);
			}

			LOG_CONTROLLER.info("TOTAL RESULT DATA [" + userModel.size() + " data ]");

		} catch (Exception e) {
			LOG_CONTROLLER.info("Failed Get data with problem is : " + e);
		}

		return userModel;
	}

	@Override
	public List<Variant> getVariantList() {
		List<Object[]> form1Objects = reportRepository.findAllUserDataVariant();
		List<Variant> userModel = new ArrayList<Variant>();

		try {

			for (Object object[] : form1Objects) {

				Variant model = new Variant();
				model.setId((int) object[0]);
				model.setCreatedDate((Date) object[1]);
				model.setVariant_name((String) object[2]);
				model.setStatus((String) object[3]);
				model.setBarcode_lenght((int) object[4]);
				model.setSpecial_character((String) object[5]);

				userModel.add(model);
			}

			LOG_CONTROLLER.info("TOTAL VARIANT DATA [" + userModel.size() + " data ]");

		} catch (Exception e) {
			LOG_CONTROLLER.info("Failed Get data with problem is : " + e);
		}

		return userModel;
	}

	public List<Variant> getVariantListWithActive() {
		List<Object[]> form1Objects = reportRepository.findAllUserDataVariantWithActive();
		List<Variant> userModel = new ArrayList<Variant>();

		try {

			for (Object object[] : form1Objects) {

				Variant model = new Variant();
				model.setId((int) object[0]);
				model.setCreatedDate((Date) object[1]);
				model.setVariant_name((String) object[2]);
				model.setStatus((String) object[3]);
				model.setBarcode_lenght((int) object[4]);
				model.setSpecial_character((String) object[5]);

				userModel.add(model);
			}

			LOG_CONTROLLER.info("TOTAL VARIANT DATA YANG ACTIVE [" + userModel.size() + " data ]");

		} catch (Exception e) {
			LOG_CONTROLLER.info("Failed Get data with problem is : " + e);
		}

		return userModel;
	}

	@Override
	public List<DirectoryCSV> getDirectoryList() {

		List<Object[]> form1Objects = reportRepository.findAllDirectoryCSV();
		List<DirectoryCSV> userModel = new ArrayList<DirectoryCSV>();

		try {

			for (Object object[] : form1Objects) {

				DirectoryCSV model = new DirectoryCSV();
				model.setId((int) object[0]);
				model.setFolderPath((String) object[1]);

				userModel.add(model);

				LOG_CONTROLLER.info("DAPATKAN DATA DIRECTORY CSV DENGAN NAMA FOLDER -> " + model.getFolderPath());
			}

		} catch (Exception e) {
			LOG_CONTROLLER.info("Failed Get data with problem is : " + e);
		}

		return userModel;
	}

	@Transactional(readOnly = false)
	public void save(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		dao.save(user);
	}

	@Transactional(readOnly = false)
	public void saveVariant(Variant variant) {
		variant.setVariant_name(variant.getVariant_name().replace(" ", "_"));
		variantDao.save(variant);
	}

	@Transactional(readOnly = false)
	public void saveDirectory(DirectoryCSV directoryCSV) {
		DirectoryCSV entity = directoryCSVDao.findById(directoryCSV.getId());

		if (entity != null) {
			entity.setFolderPath(directoryCSV.getFolderPath());
		}
	}

	public User findById(int id) {
		return dao.findById(id);
	}

	public Variant findByIdVariant(int id) {
		return variantDao.findByIdVariant(id);
	}

	public User findBySso(String sso) {
		return dao.findBySSO(sso);
	}

	@Transactional(readOnly = false)
	public void updateUser(User user) {
		User entity = dao.findById(user.getId());
		if (entity != null) {
			entity.setSsoId(user.getSsoId());
			if (!user.getPassword().equals(entity.getPassword())) {
				entity.setPassword(passwordEncoder.encode(user.getPassword()));
			}
			entity.setFirstName(user.getFirstName());
			entity.setLastName(user.getLastName());
			entity.setEmail(user.getEmail());
			entity.setLevel(user.getLevel());
			entity.setUserProfiles(user.getUserProfiles());
		}
	}

	@Transactional(readOnly = false)
	public void deleteUserByID(int sso) {
		dao.deleteByID(sso);

	}

	@Transactional(readOnly = false)
	public void deleteUserByIDVariant(int id) {
		variantDao.deleteByID(id);

	}

	@Transactional(readOnly = false)
	public void updateUserVariant(Variant variant) {
		Variant entity = variantDao.findByIdVariant(variant.getId());
		if (entity != null) {
			entity.setCreatedDate(variant.getCreatedDate());
			entity.setVariant_name(variant.getVariant_name().replace(" ", "_"));
			entity.setStatus(variant.getStatus());
			entity.setBarcode_lenght(variant.getBarcode_lenght());
			entity.setSpecial_character(variant.getSpecial_character());
		}

	}

	public boolean validateSpecialCharacter(String specialChar) {

		/* TRY TO VALIDATE SPECIAL CHARACTER */
		List<Variant> userData = getVariantList();

		for (int i = 0; i < userData.size(); i++) {

			if (specialChar.equals(userData.get(i).getSpecial_character())) {

				return true;

			}

		}

		return false;

	}

}