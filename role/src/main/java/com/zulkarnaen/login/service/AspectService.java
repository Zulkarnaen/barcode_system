package com.zulkarnaen.login.service;

/**
 * 
 * @author zulkarnaen
 *
 */
public interface AspectService {

	public boolean compareBarcode(String barcode, String username);

}