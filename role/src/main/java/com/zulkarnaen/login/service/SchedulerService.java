package com.zulkarnaen.login.service;

import java.util.Date;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.zulkarnaen.login.model.Result;

@Component
public class SchedulerService {

	org.slf4j.Logger LOG_CONTROLLER = LoggerFactory.getLogger("auditcontroller");

	@Autowired
	CsvService csvService;

	@Autowired
	private ReportService reportService;

	/* SCHEDULER SETIAP JAM 6 SORE DI SENIN - SABTU */
	@Scheduled(cron = "0 0 18 ? * MON,TUE,WED,THU,FRI,SAT")
//	@Scheduled(cron = "10 * * ? * *")
	public void inquiryListAccountSource() throws Exception {
		LOG_CONTROLLER.info("========================= Scheduler Starting.. =========================");

		boolean resultCSV = false;
		boolean hasilCSV = false;
		Date date = new Date();

		try {
			/* RELOAD UNTUK AMBIL DATA BERDASARKAN DATE */
			List<Result> result = reportService.getResultList(date);
			/* DOWNLOAD CSV */
			resultCSV = csvService.downloadCSV(result, date, hasilCSV);

			if (resultCSV == true) {
				LOG_CONTROLLER.info("Download CSV pada tanggal " + date + " BERHASIl!");
			} else {
				LOG_CONTROLLER.error("Download CSV Pada Tanggal " + date + " GAGAL!");
			}

		} catch (Exception e) {
			LOG_CONTROLLER.error("FAILED DOWNLOAD CSV FILE");

		}
		LOG_CONTROLLER.info("=======================================================================\n");
	}

}
