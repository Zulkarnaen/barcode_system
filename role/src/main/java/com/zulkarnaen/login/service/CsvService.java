package com.zulkarnaen.login.service;

import java.util.Date;
import java.util.List;

import com.zulkarnaen.login.model.Result;

public interface CsvService {

	public boolean downloadCSV(List<Result> result, Date date, boolean hasil);

}
