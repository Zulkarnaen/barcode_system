package com.zulkarnaen.login.service;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zulkarnaen.login.model.Result;
import com.zulkarnaen.login.model.Variant;
import com.zulkarnaen.login.repository.ResultDao;

/**
 * 
 * @author zulkarnaen
 *
 */
@Service("aspectService")
@Transactional(readOnly = true)
public class AspectServiceImpl implements AspectService {

	org.slf4j.Logger LOG_CONTROLLER = LoggerFactory.getLogger("auditservice");

	@Autowired
	private ReportService reportService;

	@Autowired
	private ResultDao resultDao;

	@Transactional(readOnly = false)
	public boolean compareBarcode(String barcode, String userName) {
		Date date = new Date();
		/* GET ALL DATA */
		List<Variant> variants = reportService.getVariantListWithActive();
		Result result = new Result();

		try {

			for (Variant variantEntity : variants) {
				int length = barcode.length();
				int length2 = variantEntity.getBarcode_lenght();
				if (length == length2) {
					// MATCHER INI PAKAI REGEX, JADI # HARUS KAMU GANTI DL PAKAI .
					// DIA NGGAK AKAN KENALI KLO # HARUS DI REPALCE DL
					String template = variantEntity.getSpecial_character();
					template = template.replace("#", ".");
					Pattern pattern = Pattern.compile(template);

					Matcher matcher = pattern.matcher(barcode);
					boolean isMatch = matcher.find();

					if (isMatch) {
						result.setVariantName(variantEntity.getVariant_name());
						result.setCreatedDate(date);
						result.setSerialNumber(barcode);
						result.setUserName(userName);

						Variant variant = new Variant();
						variant.setBarcode_lenght(12);

						/* SAVE RESULT */
						resultDao.save(result);

						return true;
					}

				}
			}

		} catch (Exception e) {
			LOG_CONTROLLER.error("FAILED SAVE RESULT ON " + date + " With Description " + e);
			return false;
		}
		return false;

	}

}