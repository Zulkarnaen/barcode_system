package com.zulkarnaen.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.zulkarnaen.login.model.UserProfile;
import com.zulkarnaen.login.service.UserProfileService;

/**
 * 
 * @author zulkarnaen
 *
 */
@Component
public class RoleToUserProfileConverter implements Converter<Object, UserProfile> {

	@Autowired
	UserProfileService userProfileService;

	/* TODO: GET FIND USER */
	public UserProfile convert(Object element) {
		Integer id = Integer.parseInt((String) element);
		UserProfile profile = userProfileService.findById(id);
		return profile;
	}

}