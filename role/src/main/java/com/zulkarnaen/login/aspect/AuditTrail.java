package com.zulkarnaen.login.aspect;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

/**
 * 
 * @author zulkarnaen
 *
 */
/* INI ASPECT MASIH GA DIPERLUKAN SLUR */
@Aspect
public class AuditTrail {
	org.slf4j.Logger LOG_SERVICE = LoggerFactory.getLogger("auditservice");

	private String localPath = "C:/temp/";
	private String remotePath = "/export/home/madan/";
	private String ipRemote = "localhost";
	private int port = 1234;
	private String userName = "";
	private String password = "";

	/**
	 * 
	 * @param joinPoint
	 * @throws Exception
	 */

	/* FTP */
	@AfterReturning(pointcut = "execution(* com.zulkarnaen.login.service.AspectService.compareBarcode(..))", returning = "result")
	public void afterReturnThrowingInterceptor(JoinPoint joinPoint, Object result) {

		try {

			System.out.println("Aspect jalan bos");

			/* SEND FILE */
//			FTP ftp = new FTP(ipRemote, port, userName, password);
//			ftp.upload(localPath + "filetoupload.txt", remotePath);

		} catch (Exception e) {
			LOG_SERVICE.error("FAILED DOWNLOAD CSV FILE");
		}

	}

	/* SFTP */

	private String remoteHost = "HOST_NAME_HERE";
	private String username = "USERNAME_HERE";
	private String passwords = "PASSWORD_HERE";

	/**
	 * 
	 * @param joinPoint
	 * @param request
	 * @param result
	 * @throws IOException
	 */
//	@AfterReturning(pointcut = "(execution(* com.siemo.autodebit.service.*.*(..))) && args(request)", returning = "result")
	public void afterReturnThrowingInterceptorSFTP(JoinPoint joinPoint, Object request, Object result,
			HttpServletResponse response) throws IOException {

		ChannelSftp channelSftp;
		try {

			String url = "csv";
			response.sendRedirect(url);

			/* CONNECT TO SERVER REMOTE */
			channelSftp = setupJsch();
			channelSftp.connect();

			/* SET LOCAL FILE AND REMOTE LOCATION */
			String localFile = "src/main/resources/sample.txt";
			String remoteDir = "remote_sftp_test/";
			channelSftp.put(localFile, remoteDir + "jschFile.txt");

			/* THEN CLOSE */
			channelSftp.exit();

		} catch (JSchException e) {
			// TODO Auto-generated catch block
			LOG_SERVICE.error("Masalah Dalam SFTP JSC Exception dengan detail + " + e);
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			LOG_SERVICE.error("Masalah Dalam SEND SFTP DATA Exception dengan detail + " + e);
		} catch (Exception e) {
			LOG_SERVICE.error("FAILED DOWNLOAD CSV FILE");

		}

	}

	/**
	 * 
	 * @return
	 * @throws JSchException
	 */
	private ChannelSftp setupJsch() throws JSchException {
		JSch jsch = new JSch();
		jsch.setKnownHosts("/Users/john/.ssh/known_hosts");
		Session jschSession = jsch.getSession(username, remoteHost);
		jschSession.setPassword(passwords);
		jschSession.connect();
		return (ChannelSftp) jschSession.openChannel("sftp");
	}
}